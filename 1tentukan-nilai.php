<?php
function tentukan_nilai($number)
{
    //  kode disini
    if($number>=90){
        return "nilai $number //Sangat Baik<br>";
    }else if ($number>=70 && $number<90){
        return "nilai $number //Baik<br>";
    }else if ($number>=50 && $number<70){
        return "nilai $number //Cukup<br>";
    }else{
        return "nilai $number //Kurang<br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
