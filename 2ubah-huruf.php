<?php
function ubah_huruf($string){
//kode di sini
$string = strtolower($string);
    $distance = 1;
    $result = array();
    $characters = str_split($string);

    foreach ($characters as $id => $char) {
        $result[$id] = chr(97 + (ord($char) - 97 + $distance) % 26);
    }   

    return "ubah_huruf : $string => " . join("", $result) . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
